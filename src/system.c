/*Usart initialize function definition*/
#include "system.h"

/******************************************************************************
* Name:           SYSTEM_Init
* Description:    initializes USART with selceted pins using STM libraries
* Input:          void, only 2 pins are needed and no point in having multiple between pins
				  because not all can be configured as such
* Output:         void, no output needed after initialization                  
* Timing:         *TO DO*
* Advice:         
* Note:           
* Warning:        not all of the 6 USART can be configured by user
******************************************************************************/
void USART2_Init( void )
{
  GPIO_InitTypeDef GPIO_InitStructure; //pin initialization structure
  USART_InitTypeDef USART_InitStruct; //usart initialization structure
  NVIC_InitTypeDef NVIC_InitStructure; //interrupt initialization


  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE); //enable peripherial clk
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);	//enable PORTA clk

  /*GPIO_PinAFConfig used to change a I/O pin to do specific output ex:timer,CAN,I2C,etc.*/
  
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);  //pa2=TX

  GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);	//PA3=RX
  
  /*Pin Configuration*/
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;//set to Altarnate Function Mode;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//Output Frequency
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//Output type topush-pull
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//Pin works on pull-up
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;//Target pin
  GPIO_Init(GPIOA, &GPIO_InitStructure);//functie care initializeaza pinul 2 folosind informatiile introduse in structura
  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;//Second target pin
  GPIO_Init(GPIOA, &GPIO_InitStructure);
   
  /*Usart initialization*/  
  USART_StructInit(&USART_InitStruct); //default USART parameters
  USART_Init(USART2,&USART_InitStruct); 


  /* NVIC configuration */
  /* Configure the Priority Group to 2 bits */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  
  /* Enable the USARTx Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  
  
   USART_Cmd(USART2,ENABLE); //enables USART
}

void PING_Init(void)
{
      /*
   *
   *
   * Timer configuration.
   * this one is used for PING timeout.
   */
  if (SysTick_Config(SystemCoreClock / 1000))
  { 
    /* Capture error */ 
    while (1);
  }
     //SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);
}
