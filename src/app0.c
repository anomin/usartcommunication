
#include "app0.h"
#include "types.h"
#include "util.h"
#include "msg.h"

#define APP0_ID             0

uint8_t APP0_CmdA;
uint8_t APP0_CmdB;
uint8_t APP0_Data;
uint8_t APP0_Sender;
uint8_t APP0_Buff[ MSG_LEN_DATA ];

uint8_t APP0_RecvMsgCnt;

void APP0_Init( void )
{
  /*THINK*/  
}

void APP0_Process( void )
{
  if( TRUE == MSG_Recv( APP0_ID, &APP0_Sender, APP0_Buff ) ) 
  {
    // proposal
    APP0_CmdA = UTIL_AscHexToUInt8( APP0_Buff, 1 );
    APP0_CmdB = UTIL_AscHexToUInt8( APP0_Buff+1, 1 );
    APP0_Data = UTIL_AscHexToUInt8( APP0_Buff+2, 2 );
    
    APP0_RecvMsgCnt++;
    
    APP0_Buff[ 0 ] = 'M';
    APP0_Buff[ 1 ] = 'R';
    
    UTIL_UInt8ToAscHex( APP0_RecvMsgCnt , APP0_Buff+2 );
    
    MSG_Send( APP0_ID, APP0_Sender, APP0_Buff );
    
  }
}
