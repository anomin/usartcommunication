/*Convertor functions definitions*/
#include "types.h"
#include "util.h"

static const uint8_t UTIL_HexTable[] = "0123456789ABCDEF";

/******************************************************************************
* Name:           UTIL_MemCpy
* Description:    Makes a copy of data from source to destination
* Input:          src:pointer to memory block from which to start copying;
				  dest:pointer to destination memory block where data will be sent;
				  len:lenght of memory bloc;
* Timing:         *TO DO*
* Advice:         <optional>
* Note:           <optional>
* Warning:        neither src or dest must be NULL
******************************************************************************/
void UTIL_MemCpy( uint8_t * src, uint8_t * dest, uint8_t len )
{
  while( len-- )
  {
    *dest++ = *src++;
  }
}

/******************************************************************************
* Name:           UTIL_UInt8ToAscHex
* Description:    converts 8bit character to 8bit HEX Ascii character
* Input:          n:character from message;
				  data:pointer to character vector
* Output:         void,results are stored in vector pointed by data                   
* Timing:         *TO DO*
* Advice:         <optional>
* Note:           only character from '0'->'9' and 'A'->'z' must be sent
* Warning:        <optional>
******************************************************************************/
void UTIL_UInt8ToAscHex( uint8_t n, uint8_t * data )
{
  data[ 1 ] = UTIL_HexTable[ n & 0x0F ];//
  data[ 0 ] = UTIL_HexTable[ ( n & 0xF0 ) >> 4 ];
}

/******************************************************************************
* Name:           UTIL_UInt16ToAscHex
* Description:    converts 16bit character to 16 bit HEX Ascii character
* Input:          n:character from message;
				  data:pointer to character vector;
* Output:         void,results are stored in vector pointed by data                   
* Timing:         *TO DO*
* Advice:         <optional>
* Note:           only character from '0'->'9' and 'A'->'z' must be sent
* Warning:        <optional>
******************************************************************************/
void UTIL_UInt16ToAscHex( uint16_t n, uint8_t * data )
{
  data[ 3 ] = UTIL_HexTable[ n & 0x0000F ];
  data[ 2 ] = UTIL_HexTable[ ( n & 0x00F0 ) >> 4 ];
  data[ 1 ] = UTIL_HexTable[ ( n & 0x0F00 ) >> 8 ];
  data[ 0 ] = UTIL_HexTable[ ( n & 0xF000 ) >> 12 ];
}

/******************************************************************************
* Name:           UTIL_AscHexToUInt8
* Description:    converts 8bit HEX Ascii character to 8bit character
* Input:          data:pointer to character vector;
				  pos:number of values in data;
* Output:         ret:caracter converted from Hex Ascii form                   
* Timing:         *TO DO*
* Advice:         if data vector contains 0 values then pos can be reduced thus reduces runtime
* Note:           <optional>
* Warning:        <optional>
******************************************************************************/
uint8_t UTIL_AscHexToUInt8( uint8_t * data, uint8_t pos )
{
  uint8_t ret = 0;
  uint8_t i = 0;
  
  while( pos )
  {
    ret <<= 4;
    ret += ( data[ i ] > '9' ) ? ( data[ i ] - 'A' + 10 ) : ( data[ i ] - '0' );
    i++;
    pos--;
  }
  
  return ret;
}

/******************************************************************************
* Name:           UTIL_AscHexToUInt16
* Description:    converts 16bit HEX Ascii character to 16bit character
* Input:          data:pointer to character vector;
				  pos:number of values in data;
* Output:         ret:caracter converted from Hex Ascii form                   
* Timing:         *TO DO*
* Advice:         if data vector contains 0 values then pos can be reduced thus reduces runtime
* Note:           <optional>
* Warning:        <optional>
******************************************************************************/
uint16_t UTIL_AscHexToUInt16( uint8_t * data, uint8_t pos )
{
  uint16_t ret = 0;
  uint8_t i = 0;
  
  while( pos )
  {
    ret <<= 4;
    ret += ( data[ i ] > '9' ) ? ( data[ i ] - 'A' + 10 ) : ( data[ i ] - '0' );
    i++;
    pos--;
  }
  return ret;
}
