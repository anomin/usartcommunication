/*Header for Message descripion and Msg functions prototypes*/
#ifndef __MSG_H__
#define __MSG_H__

/*start of message*/
#define MSG_EOM             '>'
/* end of message */
#define MSG_SOM             '<'

/*Important Note: Values TO be changed for prefered message lenght*/
#define MSG_LEN_DATA        4
#define MSG_LEN_CTRL        6

#define MSG_LEN_TRSP        (MSG_LEN_DATA + MSG_LEN_CTRL)

#define MSG_LEN_NET         (MSG_LEN_DATA + MSG_LEN_CTRL - 2) 

/*Message descriptors*/
#define MSG_ADDR_SRC_IDX    0
#define MSG_ADDR_DEST_IDX   (MSG_ADDR_SRC_IDX   + 1)
#define MSG_APP_ID_IDX      (MSG_ADDR_DEST_IDX  + 1)
#define MSG_DATA_START_IDX  (MSG_APP_ID_IDX     + 1)
#define MSG_CRC_IDX         (MSG_DATA_START_IDX + MSG_LEN_DATA)

/*Message functions prototypes*/
uint8_t MSG_Recv( uint8_t app,/* OUTPUT */uint8_t *src, /*OUTPUT*/ uint8_t * data );
uint8_t MSG_Send( uint8_t app, uint8_t dest, uint8_t * data );

#endif
