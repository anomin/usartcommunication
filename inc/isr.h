
#ifndef __ISR_H__
#define __ISR_H__
#define FALSE 0
#include "msg.h"


#define ISR_TX_BUFFER_LEN MSG_LEN_TRSP 

extern volatile uint8_t ISR_TXBusy;

extern uint8_t ISR_TXBuffer[ ISR_TX_BUFFER_LEN ];

extern volatile uint8_t ISR_RXBufferReady;
extern uint8_t * ISR_RXBufferRD;

void ISR_Init( void );

#endif
