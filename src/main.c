#include "stm32f4xx.h"

#include "types.h"
#include "util.h"
#include "isr.h"
#include "msg.h"
#include "system.h"
#include "queue.h"
#include "net.h"
#include "app0.h"
#include "app1.h" 
#include "app2.h"
  
int main()
{
   USART2_Init();
   ISR_Init();
   NET_Init();


   while(1)
   {
   //for (i=0;i<100000;i++);
     //USART_SendData(USART2,0xAA);
	  //data=USART_ReceiveData(USART2);
	  APP0_Process();
	  APP1_Process();
     NET_Process();
     APP2_Process(1000,'5');
	}
}
