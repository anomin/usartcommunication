/*include ordin is trivial!*/
#include "stm32f4xx.h"
#include "types.h"

#include "isr.h"
#include "util.h"
#include "msg.h"

extern uint16_t TimeOut;

#define ISR_RX_BUFFER_LEN MSG_LEN_NET 

enum { ISR_RX_IDLE, ISR_RX_RECV };

uint8_t ISR_TXBuffer[ ISR_TX_BUFFER_LEN ];
uint8_t ISR_TXBufferIdx = 0;

volatile uint8_t ISR_TXBusy = FALSE;

volatile uint8_t ISR_RXBufferReady;
uint8_t ISR_RXBufferWRIdx;

uint8_t * ISR_RXBufferRD;
static uint8_t * ISR_RXBufferWR;

static uint8_t ISR_Buff0[ ISR_RX_BUFFER_LEN ];
static uint8_t ISR_Buff1[ ISR_RX_BUFFER_LEN ];

static uint8_t * p_aux;

static uint8_t ISR_RXState = ISR_RX_IDLE;

/******************************************************************************
* Name:           ISR_Init
* Description:    Initializesthe interupts usedin the USART comunicationon PORT2
* Input:          void
		  
* Output:         void                   
* Timing:         <max. cycles/mean cycles/min. cycles>
* Advice:         <optional>
* Note:           *TO DO* function that initalizes any other PORT that is accesible to users 
* Warning:        Output lvl is 0-3.3V need RS232 driver for comunication with COM1 PORT
******************************************************************************/
void ISR_Init( void )
{
  USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
  USART_ITConfig(USART2, USART_IT_TC, ENABLE);

  ISR_RXBufferRD = ISR_Buff0;
  ISR_RXBufferWR = ISR_Buff1;
}

// USART2 interrupr handling
/* usart IRQHandler handles every interupt 
 * checking it by the return flag from USART_GetITStatus
 *
 */
void USART2_IRQHandler(void)
{
	uint8_t data;
  
  /* THINK: PARITY_ERR, OVERRUN_ERR, FRAMING_ERR */
  /* TIMEOUTS */

	if (USART_GetITStatus(USART2, USART_IT_RXNE) == SET)
	{
  		/* clear recive interrupt */
		USART_ClearITPendingBit(USART2,USART_IT_RXNE);
		data = USART_ReceiveData(USART2);
   
		switch( ISR_RXState )
		{
			case ISR_RX_IDLE:
				if( MSG_SOM == data )
				{
				ISR_RXBufferWRIdx = 0;
				ISR_RXState = ISR_RX_RECV; 
				}
			break;
			case ISR_RX_RECV:
			if( MSG_EOM == data && ISR_RXBufferWRIdx == ISR_RX_BUFFER_LEN /*THINK*/ )
			{
				ISR_RXBufferReady = TRUE;
				p_aux = ISR_RXBufferRD;
				ISR_RXBufferRD = ISR_RXBufferWR;
				ISR_RXBufferWR = p_aux;
				ISR_RXState = ISR_RX_IDLE;
			}
			else if( ISR_RXBufferWRIdx < ISR_RX_BUFFER_LEN )
			{ 
				/*THINK*/
				if( MSG_EOM != data && MSG_SOM != data )
				{
					ISR_RXBufferWR[ ISR_RXBufferWRIdx ] = data; 
					ISR_RXBufferWRIdx++;
				}
				else if( MSG_EOM == data )
				{	
					ISR_RXState = ISR_RX_IDLE;
					/*THINK*/ /* MAY COUNT ERRORS ERR_CNT++ */
				}
				else
				{
					ISR_RXBufferWRIdx = 0;
				}
			}
			else
			{
				ISR_RXState = ISR_RX_IDLE;
			}
			break;
		}//end case
	}//end if RX
	if(USART_GetITStatus(USART2, USART_IT_TC) == SET)
	{
     		/* clear transmit interrupt */
		USART_ClearITPendingBit(USART2,USART_IT_TC);
		if( ISR_TXBufferIdx < ISR_TX_BUFFER_LEN )
		{	
			USART_SendData(USART2, ISR_TXBuffer[ ISR_TXBufferIdx ]);
			ISR_TXBufferIdx++;
		}
		else
		{
			ISR_TXBufferIdx = 1;
			ISR_TXBusy = FALSE;
		}
	}
}
  void SysTick_Handler(void)
{            
  if (TimeOut!=0x0001)
     TimeOut--;
}
