
#ifndef __NET_H__
#define __NET_H__

#include "queue.h"

#define NET_OWN_ADDRESS '4'

#define NET_APPS_NO       2

extern queue_t NET_RXQueue[ NET_APPS_NO ] ;
extern queue_t NET_TXQueue;

void NET_Init( void );
void NET_Process( void );

#endif
