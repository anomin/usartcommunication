/*Header with queue structure definition and funtion prototypes*/
#ifndef __QUEUE_H__
#define __QUEUE_H__

typedef struct queue {
  uint8_t len;
  uint8_t wr_idx;
  uint8_t rd_idx;
  uint8_t count;
  uint8_t ** buff; 
} queue_t;

/*Queue function prototypes*/
void QUEUE_Init( queue_t * q, uint8_t len, uint8_t ** buff );
uint8_t QUEUE_IsFull( queue_t * q );
uint8_t QUEUE_IsEmpty( queue_t * q );
void QUEUE_Put( queue_t * q, uint8_t * src );
void QUEUE_Get( queue_t * q, uint8_t * dest );

#endif
