/*Queue functions definitions*/

#include "types.h"
#include "util.h"
#include "queue.h"
#include "msg.h"

/******************************************************************************
* Name:           QUEUE_Init
* Description:    can be considered a constructor for queue structure
* Input:          q:structure that is going to be initialized;
				  len:lenght of queue;
		          buff:matrix that is going to store the queue data;
* Output:         void,no output needed                  
* Timing:         *TO DO*
* Advice:         <optional>
* Note:           <optional>
* Warning:        <optional>
******************************************************************************/
void QUEUE_Init( queue_t * q, uint8_t len, uint8_t ** buff )
{
  q->len = len;
  q->wr_idx = 0;
  q->rd_idx = 0;  
  q->count = 0;
  q->buff = buff;
}

/******************************************************************************
* Name:           QUEUE_Get
* Description:    copies queue to specified memory block
* Input:          q:structure from which data will be stored on a memory block;
				  dest:adress of the memory block;
* Output:         void,no output needed, data will be stored in q                 
* Timing:         *TO DO*
* Advice:         <optional>
* Note:           <optional>
* Warning:        <optional>
******************************************************************************/
PUBLIC void QUEUE_Get( /*INOUT */queue_t * q,/* OUTPUT */uint8_t * dest )
{
  UTIL_MemCpy( q->buff[ q->rd_idx ], (dest), MSG_LEN_NET );
  q->rd_idx = ( q->rd_idx == q->len - 1 ) ? 0 : ( q->rd_idx + 1 );
  q->count--;
}

/******************************************************************************
* Name:           QUEUE_Put
* Description:    copies queue from specified memory block
* Input:          q:structure that will store data from the memory block;
		          src:adress of the memory block;
* Output:         void,no output needed, data will be stored in q                 
* Timing:         *TO DO*
* Advice:         <optional>
* Note:           <optional>
* Warning:        <optional>
******************************************************************************/
void QUEUE_Put( /*INOUT */queue_t * q, /*INPUT */uint8_t * src )
{
  UTIL_MemCpy( (src), q->buff[ q->wr_idx ], MSG_LEN_NET );
  q->wr_idx = ( q->wr_idx == q->len - 1 ) ? 0 : ( q->wr_idx + 1 );
  q->count++;
}

/******************************************************************************
* Name:           QUEUE_IsEmpty
* Description:    checks if queue is empty
* Input:          q:structure that is going to be cheked if empty
* Output:         1 if empty and 0 if q has atleast 1 one element or full                 
* Timing:         *TO DO*
* Advice:         <optional>
* Note:           <optional>
* Warning:        <optional>
******************************************************************************/
uint8_t QUEUE_IsEmpty( queue_t * q )
{
  return ( q->count == 0 );  
}

/******************************************************************************
* Name:           QUEUE_IsFull
* Description:    checks if queue is full
* Input:          q:structure that is going to be cheked if full
* Output:         1 if queue is full and 0 in any other case                 
* Timing:         *TO DO*
* Advice:         <optional>
* Note:           <optional>
* Warning:        <optional>
******************************************************************************/
uint8_t QUEUE_IsFull( queue_t * q )
{
  return ( q->count == q->len );  
}
