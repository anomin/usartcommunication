
#include "types.h"
#include "util.h"
#include "queue.h"
#include "net.h"
#include "crc.h"
#include "msg.h"

static uint8_t MSG_MsgBuffer[ MSG_LEN_NET ];

/******************************************************************************
* Name:           MSG_Send
* Description:    checks if transmit queue is full,adds specific descriptors to the message
				  ,convers character to be sent to asciihex,sends message to transmit queue
* Input:          app:application id;
			      dest:destination id;
				  data:data that will be sent
* Output:         returns 0 if treansmit queue is full and 1 if the message will be sent                
* Timing:         <max. cycles/mean cycles/min. cycles>
* Advice:         <optional>
* Note:           <optional>
* Warning:        <optional>
******************************************************************************/
uint8_t MSG_Send( uint8_t app, uint8_t dest, uint8_t * data )
{
  uint8_t ret;
  uint8_t tmp[2];
  ret = QUEUE_IsFull( &NET_TXQueue ); 
  if( !ret )
  {
    UTIL_UInt8ToAscHex( app, tmp );
    MSG_MsgBuffer[ MSG_APP_ID_IDX ] = tmp[ 1 ];

    UTIL_UInt8ToAscHex( NET_OWN_ADDRESS, tmp);
    MSG_MsgBuffer[ MSG_ADDR_SRC_IDX  ] = tmp[ 1 ];

    UTIL_UInt8ToAscHex( dest, tmp );
    MSG_MsgBuffer[ MSG_ADDR_DEST_IDX ] = tmp[ 1 ];
    
    UTIL_MemCpy( data, &MSG_MsgBuffer[ MSG_DATA_START_IDX ] , MSG_LEN_DATA );
    
    UTIL_UInt8ToAscHex( CRC_ComputeCRC( MSG_MsgBuffer ), tmp ); 
    MSG_MsgBuffer[ MSG_CRC_IDX ] = tmp[ 1 ];
    
    QUEUE_Put( &NET_TXQueue, MSG_MsgBuffer );  
  }
  return ret;
}

/******************************************************************************
* Name:           MSG_Send
* Description:    checks if receive queue is empty, receives queue from application  specified
				  and stores it in message buffer, then translates the message from ascii
* Input:          app:application id;
			      src:source id;
				  data:data that will be received
* Output:         returns 0 if receive queue is full and 1 if the message will be received                   
* Timing:         <max. cycles/mean cycles/min. cycles>
* Advice:         <optional>
* Note:           <optional>
* Warning:        <optional>
******************************************************************************/
uint8_t MSG_Recv( uint8_t app,/* OUTPUT */uint8_t *src,/* OUTPUT */uint8_t * data )
{
  uint8_t ret;
  ret = QUEUE_IsEmpty( &NET_RXQueue[ app ] ); 
  if( !ret )
  {
    QUEUE_Get( &NET_RXQueue[ app ], MSG_MsgBuffer );
    *src = UTIL_AscHexToUInt8( &MSG_MsgBuffer[ MSG_ADDR_SRC_IDX ], 1 );
    UTIL_MemCpy( &MSG_MsgBuffer[ MSG_DATA_START_IDX ], data, MSG_LEN_DATA );
  }
  return !ret;
}
