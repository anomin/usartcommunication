
#ifndef __APP2_H__
#define __APP2_H__



/* return enum. SUCCES - on response
 *               ERROR - on error of any kind
 *               REQUEST_TIMED_OUT - on request timed out
 *               SEARCH - is still searching.timer is tiking
 */
 typedef enum {PING_OK, PING_ERROR, REQUEST_TIMED_OUT, PING_SEARCH} PING_ERR;

/* defines for TimeOut value */
#define ZERO_VALUE   0x0000
#define START_VALUE 0x0001

#define APP2_ID             2

PING_ERR APP2_Process(uint16_t t_out,uint8_t destionation);
void APP2_Init(void);

#endif
