/*Header ASCIIHex-uint8/16 functions*/
#ifndef __UTIL_H__
#define __UTIL_H__

#define TRUE      1
#define FALSE     0

#define SUCCESS   1
#define FAIL      0

//#define INPUT
//#define OUTPUT
//#define INOUT

#define PUBLIC    extern
#define PRIVATE   static

void UTIL_MemCpy( uint8_t * src, uint8_t * dest, uint8_t len );
/*Converting functions prototypes*/
void UTIL_UInt16ToAscHex( uint16_t n, uint8_t * data );
void UTIL_UInt8ToAscHex( uint8_t n, uint8_t * data );
uint8_t UTIL_AscHexToUInt8( uint8_t * data, uint8_t pos );
uint16_t UTIL_AscHexToUInt16( uint8_t * data, uint8_t pos );

#endif
