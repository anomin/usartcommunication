/*Integer type definitions*/

#ifndef __TYPES_H__
#define __TYPES_H__

typedef unsigned char   uint8_t;
//typedef signed char     sint8_t;

typedef unsigned short  uint16_t;
//typedef signed short    sint16_t;

#endif
