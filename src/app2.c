#include "system.h"                     
#include "app2.h"
#include "types.h"
#include "util.h"
#include "msg.h"
#include "net.h"

uint8_t APP2_Sender;
uint8_t APP2_Buff[ MSG_LEN_DATA ];

uint8_t APP2_RecvMsgCnt;
uint16_t TimeOut;



/******************************************************************************
* Name:           APP2_Init
* Description:    Initializes timer la 1ms
* Input:          void
* Output:         void                   
* Timing:         
* Advice:         <optional>
* Note:           Implement returning of PING request time
* Warning:        <optional>
******************************************************************************/
void APP2_Init( void )
{
   TimeOut=ZERO_VALUE;
     PING_Init(); 
}

/******************************************************************************
* Name:           APP2_Process
* Description:    Initializes timer la 1ms
* Input:          t_out -t ime out value for timer
*				  destination - adress to send ping to
* Output:         value from PING_ERR enumeration that describes the application status                    
* Timing:         
* Advice:         <optional>
* Note:           <optional>
* Warning:        Try not to set t_out too low, >500
******************************************************************************/
PING_ERR APP2_Process(uint16_t t_out,uint8_t destination)
{
/* testing phase */
if (destination==NET_OWN_ADDRESS || t_out <= START_VALUE) //@TO DO convert OWN_ADDRESS. SEE APP1 if ok.
{
   return PING_ERROR;
}


 /* initial phase. compose and send message */ 
 if (TimeOut==ZERO_VALUE)
 {
   APP2_Buff[0] = 'P';
   APP2_Buff[1] = 'I';
   APP2_Buff[2] = 'N';
   APP2_Buff[3] = 'G';
   MSG_Send( APP2_ID, destination, APP2_Buff );

   /* offset of 1 msec( @ref SysTick_Handler() ) */
   TimeOut=t_out+START_VALUE;
 }
 /* Timeout occured. */
 if(TimeOut==START_VALUE)
 {
    //PING REQUEST TIME OUT! DO SOMETHING!

    TimeOut=t_out+START_VALUE;
    return REQUEST_TIMED_OUT;
 }
 /* OMG! message for me. Do smth with him */
  if( TRUE == MSG_Recv( APP2_ID, &APP2_Sender, APP2_Buff ) ) 
  {
   if(APP2_Buff[0]=='R'&&APP2_Buff[1]=='E'&&APP2_Buff[2]=='S'&&APP2_Buff[3]=='P')
   {
      /* PING RESPONSE OK.DO SOMETHING. */
	  TimeOut=ZERO_VALUE;
      return PING_OK;
   }
   if(APP2_Buff[0]=='P'&&APP2_Buff[1]=='I'&&APP2_Buff[2]=='N'&&APP2_Buff[3]=='G')
   {
      /* SOMEBODY IS PINGING ME.RESPONSE. */
      APP2_Buff[0] = 'R';
   	  APP2_Buff[1] = 'E';
      APP2_Buff[2] = 'S';
      APP2_Buff[3] = 'P';
      MSG_Send( APP2_ID, APP2_Sender, APP2_Buff );
   }
   
  }
  return PING_SEARCH;
}
