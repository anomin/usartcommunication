
#include "types.h"
#include "util.h"
#include "crc.h"

/******************************************************************************
* Name:           CRC_ComputeCRC
* Description:    Checks if data has been modified in a given paremeter
* Input:          data:data to be cheked		  
* Output:         0 if data has been altered,1 if data reamined unchanged                   
* Timing:        
* Advice:         
* Note:           curently all data that will be checked will be unmodified
* Warning:        
******************************************************************************/
uint8_t crc;

uint8_t CRC_ComputeCRC( uint8_t * data )
{
   crc8_clear();
   crc8_addbyte(*data);
   return crc;
}

static unsigned char crc;

void crc8_clear(void)
    {
    crc = 0;
    }

void crc8_addbyte(unsigned char data)
    {
    unsigned char                bit_counter;
    unsigned char                feedback_bit;
    
      bit_counter = 8;
      do
        {
         feedback_bit = (crc ^ data) & 0x01;
        if (feedback_bit == 0x01)
            {
            crc = crc ^ 0x18; //0X18 = X^8+X^5+X^4+X^0
            }
         crc = (crc >> 1) & 0x7F;
         if (feedback_bit == 0x01)
            {
            crc = crc | 0x80;
            }

         data = data >> 1;

         bit_counter--;

        }while (bit_counter > 0);
        
     }

unsigned char crc8_getcrc(void)
    {
    return crc;
    }
