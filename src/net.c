
 #include "stm32f4xx.h"
//#include <ioavr.h>
//#include <inavr.h>
//#include "stm32f4xx_usart.h"
//#include "stm32f4xx_rcc.h"
#include "types.h"
#include "util.h"
#include "msg.h"
#include "isr.h"
#include "queue.h"
#include "crc.h"
#include "net.h"

#define NET_RX_QUEUE_LEN  3
#define NET_TX_QUEUE_LEN  5

// uncomment the line below in ordere to active the CRC check
// #define NET_CHECK_CRC

static uint8_t NET_RXBuff[ NET_APPS_NO * NET_RX_QUEUE_LEN * MSG_LEN_NET ];
static uint8_t NET_TXBuff[ NET_TX_QUEUE_LEN * MSG_LEN_NET ];

static uint8_t * NET_RXPtr[ NET_APPS_NO * NET_RX_QUEUE_LEN ];
static uint8_t * NET_TXPtr[ NET_TX_QUEUE_LEN ];

queue_t NET_RXQueue[ NET_APPS_NO ] ;
queue_t NET_TXQueue;
/******************************************************************************
* Name:           NET_Init
* Description:    Initializes the NET level queue for receive and transmit using
				  using the global vectors NET_TX(RX)Buff and NET_TX(RX)Ptr
* Input:          void, because vectors used in function are global 
* Output:         void, because vectors used in function are global                   
* Timing:         *TO DO*
* Advice:         
* Note:           
* Warning:        
******************************************************************************/
void NET_Init( void )
{
  uint8_t i, j;

  for( i = 0; i < NET_APPS_NO * NET_RX_QUEUE_LEN; i++ )
  {
    NET_RXPtr[ i ] = ( NET_RXBuff + i * MSG_LEN_NET );  
  }
  
  for( j = 0; j < NET_APPS_NO; j++ )
  {
    QUEUE_Init( &NET_RXQueue[ j ], NET_RX_QUEUE_LEN, NET_RXPtr + j * NET_RX_QUEUE_LEN );    
  }
  
  for( i = 0; i < NET_TX_QUEUE_LEN; i++ )
  {
    NET_TXPtr[ i ] = ( NET_TXBuff + i * MSG_LEN_NET );  
  }

  QUEUE_Init( &NET_TXQueue, NET_TX_QUEUE_LEN, NET_TXPtr );
}
/******************************************************************************
* Name:           NET_Route
* Description:    checks if the received message has own address then stores
				  it in NET RX queue
* Input:          void, because vectors used in function are global 
* Output:         void, because vectors used in function are global                   
* Timing:         *TO DO*
* Advice:         
* Note:           
* Warning:        
******************************************************************************/
PRIVATE void NET_Route( void )
{
  uint8_t q_idx;
  
  if( TRUE == ISR_RXBufferReady )
  {
    ISR_RXBufferReady = FALSE;

#ifdef NET_CHECK_CRC
    if( CRC_ComputeCRC( &ISR_RXBufferRD[ 0 ] ) == 
        UTIL_AscHexToUInt8( &ISR_RXBufferRD[ MSG_CRC_IDX ], 1) )
    {
#endif
      //critical patch, because the source of recv message would never be equal to NET_OWN_ADDRESS
      if( NET_OWN_ADDRESS == /*UTIL_AscHexToUInt8( &*/ISR_RXBufferRD[ MSG_ADDR_DEST_IDX ]/*, 1 )*/ || 0x30 == ISR_RXBufferRD[ MSG_ADDR_DEST_IDX ])
      {
        q_idx = UTIL_AscHexToUInt8( &ISR_RXBufferRD[ MSG_APP_ID_IDX ], 1 );
        QUEUE_Put( &NET_RXQueue[ q_idx ], ISR_RXBufferRD );   
      }
      else
      {
        QUEUE_Put( &NET_TXQueue, ISR_RXBufferRD );   
      }
#ifdef NET_CHECK_CRC
    }
    else
    {
      // THINK
      // just drop the message
    }
#endif
  }
}
/******************************************************************************
* Name:           NET_Send
* Description:    checks if transmit queue is empty and if TX interupt is busy,
				  if not then it sends the messege from NET TX buffer
* Input:          void, because vectors used in function are global 
* Output:         void, because vectors used in function are global                   
* Timing:         *TO DO*
* Advice:         
* Note:           
* Warning:        
******************************************************************************/
PRIVATE void NET_Send( void )
{
  if( !QUEUE_IsEmpty( &NET_TXQueue ) && ( !ISR_TXBusy ) )
  {

    ISR_TXBuffer[ 0 ] = MSG_SOM;    
	
    QUEUE_Get( &NET_TXQueue, ISR_TXBuffer + 1 );
    
    ISR_TXBuffer[ MSG_LEN_TRSP - 1 ] = MSG_EOM;
    ISR_TXBusy = TRUE;
	USART_SendData(USART2,ISR_TXBuffer[ 0 ]);
	}
}
/******************************************************************************
* Name:           NET_Process
* Description:    Function that is to be used in a permenet loop to check the flow
				  of messages
* Input:          void, because it uses functions that require no parameters 
* Output:         void                   
* Timing:         *TO DO*
* Advice:         
* Note:           
* Warning:        In case of error go to NET_Route or NET_Send function definitions
******************************************************************************/
void NET_Process( void )
{
  NET_Route();
  NET_Send();
}
