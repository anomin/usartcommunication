
#include "app1.h"
#include "types.h"
#include "util.h"
#include "msg.h"
#include "net.h"


uint8_t APP1_State;
uint8_t APP1_BCast=0x30;
uint8_t APP1_Buff[ MSG_LEN_DATA ];
uint8_t nospam=0;
uint8_t APP1_RecvMsgCnt;

uint8_t  NET_LIDER_ADDRESS=NET_OWN_ADDRESS;

/******************************************************************************
* Name:           APP1_Init
* Description:    initializes aplication state and fills unused data buffer 
				  with useless data that will not be modified, esthetics
* Input:          void, because vectors used in function are global 
* Output:         void, because vectors used in function are global                   
* Timing:         
* Advice:         
* Note:           redundant data could be changed but the other 2 data 
				  locations are frequently used by the aplication
* Warning:        
******************************************************************************/
void APP1_Init( void )
{

APP1_State=State_Init;
APP1_Buff[0]='*';
APP1_Buff[3]='*';
  
}

/******************************************************************************
* Name:           APP1_Init
* Description:    leader elect function that uses broadcast and based 
				  on mesages received makes decisions
* Input:          void, because vectors used in function are global 
* Output:         int that returns 11 when a leader has been found                   
* Timing:         
* Advice:         
* Note:           it supports new node aditions that could be leader
* Warning:        
******************************************************************************/
int APP1_Process(void)
{
  int mail;
  if (APP1_State == State_Init)
  {
  		APP1_State='S';
		APP1_Buff[1]='S';
		APP1_Buff[2]=NET_OWN_ADDRESS;
    	MSG_Send( APP1_ID, APP1_BCast, APP1_Buff );
		return 0;
  }
  mail=MSG_Recv( APP1_ID,&APP1_BCast, APP1_Buff);
  if( TRUE == mail && APP1_BCast!=NET_OWN_ADDRESS) 
  {
    	APP1_BCast=0x30;   	
    	if(APP1_State!='F')APP1_State=APP1_Buff[1];
    	if(APP1_State == 'S')
    	{

        	if (NET_LIDER_ADDRESS < APP1_Buff[2])
        	{
				NET_LIDER_ADDRESS=APP1_Buff[2];
				nospam=0;
            	MSG_Send( APP1_ID, APP1_BCast, APP1_Buff );
			}
			else if (APP1_Buff[2] < NET_LIDER_ADDRESS)
			{
				if(nospam<3)
				{
					nospam++;
					APP1_Buff[2]=NET_LIDER_ADDRESS;
					MSG_Send( APP1_ID, APP1_BCast, APP1_Buff );
				}
			}	
        	else if (APP1_Buff[2] == NET_OWN_ADDRESS)
        	{
          		APP1_Buff[1]='F';
		  		MSG_Send( APP1_ID, APP1_BCast, APP1_Buff );
          		return 0;
        	}
		 }
		 else if(APP1_State == 'F')
	 	 {
				if(APP1_Buff[2]>NET_LIDER_ADDRESS)
				{
					APP1_State='S';
					NET_LIDER_ADDRESS=APP1_Buff[2];
					nospam=0;
					MSG_Send( APP1_ID, APP1_BCast, APP1_Buff );
				}
				else if (NET_LIDER_ADDRESS<APP1_Buff[2])
				{
          			NET_LIDER_ADDRESS=APP1_Buff[2];
					nospam=0;
					MSG_Send( APP1_ID, APP1_BCast, APP1_Buff );
          			return 1;
				}
	  	}	
    }
    
  
  return 0;
}
  
